#!/bin/sh
sudo apt update
curl -fsSL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get install -y nodejs

read -p "user.name: " name
git config --global user.name "$name"
read -p "user.email: " email
git config --global user.email "$email"
